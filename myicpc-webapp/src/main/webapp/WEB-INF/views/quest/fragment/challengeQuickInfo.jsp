<%@ include file="/WEB-INF/views/includes/taglibs.jsp" %>
<h4>
     <c:if test="${not empty challenge.secondsToEndDate and challenge.secondsToEndDate gt 0}">
         <span class="pull-right">
            <span class="challenge-${challenge.id}-countdown" id="detail-challenge-${challenge.id}-countdown">
            	<script type="text/javascript">
            	$(function() {
            	    var timediff${challenge.id} = ${challenge.secondsToEndDate};
            	        if (timediff${challenge.id} >= 0) {
            	            $(".challenge-${challenge.id}-countdown").html(convertSecondsToPretty(timediff${challenge.id}));
            	        } else {
            	            $(".challenge-${challenge.id}-countdown").html('<spring:message code="quest.challenge.over" />');
            	        }
            	});   
			    </script>
            </span> 
            <spring:message code="quest.flashChallenge.left" />
         </span>
     </c:if>
     <c:if test="${not empty showName and showName}">
          ${challenge.name}
     </c:if>
</h4>
<p>

<span class="label label-success"><spring:message code="quest.challenge.points" arguments="${challenge.defaultPoints}" /></span>
<c:if test="${challenge.requiresPhoto}">
    <span class="label label-default"><spring:message code="quest.challenge.attachPhoto" /></span>
</c:if>
<c:if test="${challenge.requiresPhoto and challenge.requiresVideo}">
    <spring:message code="or" />
</c:if>
<c:if test="${challenge.requiresVideo}">
    <span class="label label-default"><spring:message code="quest.challenge.attachVideo" /></span>
</c:if>
</p>