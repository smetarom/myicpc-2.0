<%@ include file="/WEB-INF/views/includes/taglibs.jsp" %>
<%@ include file="/WEB-INF/views/quest/fragment/questInfoTimeScript.jsp" %>


<table class="table table-striped table-hover table-menu">
    <tbody>
    <c:forEach var="challenge" items="${challenges}">
        <tr>
            <td>
			    <c:if test="${not empty challenge.secondsToEndDate and challenge.secondsToEndDate gt 0}">
			         <span class="pull-right challenge-info-small">
			           <span class="challenge-${challenge.id}-countdown" id="menu-challenge-${challenge.id}-countdown">
			           </span>
			            <spring:message code="quest.flashChallenge.left" />
			         </span>
			    </c:if>
			    <c:if test="${not empty challenge.secondsToEndDate and challenge.secondsToEndDate eq 0}">
			         <span style="color: #a94442;" class="pull-right challenge-info-small ">
			           <span class="challenge-${challenge.id}-countdown" id="menu-challenge-${challenge.id}-countdown"></span> 
			            <spring:message code="quest.challenge.over" />
			         </span>
			    </c:if>
                <c:if test="${param.isMobile eq 'true'}">
                    <a href="<spring:url value="${contestURL}/quest/challenge/${challenge.id}" />">
                        <strong>${challenge.name}</strong>
                        <div class="challenge-info-small no-link">#${challenge.hashtag} &middot; <spring:message code="Xpoints" arguments="${challenge.defaultPoints}" /></div>
                    </a>
                </c:if>
                <c:if test="${param.isMobile ne 'true'}">
                    <a href="#${challenge.hashtag}" onclick="loadChallengeContent(${challenge.id})">
                        <strong>${challenge.name}</strong>
                        <div class="challenge-info-small no-link">#${challenge.hashtag} &middot; <spring:message code="Xpoints" arguments="${challenge.defaultPoints}" /></div>
                    </a>
                </c:if>
            </td>
        </tr>
    </c:forEach>
    </tbody>
</table>

