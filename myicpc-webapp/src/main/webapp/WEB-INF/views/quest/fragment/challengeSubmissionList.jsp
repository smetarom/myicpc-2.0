<%@ include file="/WEB-INF/views/includes/taglibs.jsp" %>
<table id="acceptedSubmissions" class="table table-hover table-striped">
    <tbody>
    <c:forEach var="submission" items="${submissions}" varStatus="status">
        <tr class="${status.index gt 4 ? 'additionalRow' : ''}" style="${status.index gt 4 ? 'display: none' : ''}">
            <td style="width: 50px">
                <img src="${submission.participant.contestParticipant.profilePictureUrl}"
                     class="imgICPCID"
                     alt="${submission.participant.contestParticipant.fullname}"
                     onerror="profilePictureError(this)">
            </td>
            <td style="padding-left: .4em">
                <span class="challenge-info-small" style="float:right">
                	<fmt:formatDate value="${submission.notification.timestamp}" pattern="MMM, dd H:mm a" />
                </span>
                <strong>${submission.participant.contestParticipant.fullname}</strong>
                <br/>
                <c:if test="${showSubmissions}">
	                <span class="challenge-info-small" style="">
    	                <t:questSubmission submission="${submission}" />
    	            </span>    
                </c:if>
            </td>
        </tr>
    </c:forEach>
    </tbody>
</table>

<c:if test="${submissions.size() gt 5}">
    <t:button onclick="toggleSubmissionList('acceptedSubmissions');" styleClass="btn-sm">Load more</t:button>
</c:if>
