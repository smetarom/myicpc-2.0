<%@ include file="/WEB-INF/views/includes/taglibs.jsp" %>

<t:template>
    <jsp:attribute name="headline">
        <spring:message code="nav.quest" />
    </jsp:attribute>
    <jsp:attribute name="title">
        <spring:message code="nav.quest" />
    </jsp:attribute>

    <jsp:attribute name="javascript">
        <script src="<c:url value='/js/myicpc/controllers/timeline.js'/>" defer></script>

        <script type="application/javascript">
            function timelineAcceptPost(post) {
                var supportedNotificationTypes = ["twitter"];
                if (supportedNotificationTypes.indexOf(post.type) == -1) {
                    return false;
                }
                var questHashtag = '${not empty questHashtag ? questHashtag : '#Quest'}';
                return post.body.search(new RegExp(questHashtag, "i")) != -1;
            }

            $(function() {
                Timeline.init();
                Timeline.ignoreScrolling = true;
                Timeline.acceptFunction = timelineAcceptPost;
                startSubscribe('${r.contextPath}', '${contest.code}', 'notification', updateTimeline, null);
                videoAutoplayOnScroll();

                $(window).scroll(videoAutoplayOnScroll);

            });
        </script>
        <%@ include file="/WEB-INF/views/quest/fragment/questInfoTimeScript.jsp" %>
    </jsp:attribute>

    <jsp:body>
        <%@ include file="/WEB-INF/views/quest/fragment/questInfo.jsp" %>

        <%--<c:if test="${not empty voteCandidates}">--%>
            <%--<div class="col-sm-12">--%>
                <%--<div id="vote-panel" class=" clearfix">--%>
                    <%--<div class="pull-right">--%>
                        <%--<t:button styleClass="btn-hover" onclick="$('#vote-panel').hide()"><spring:message code="quest.vote.hide" /></t:button>--%>
                    <%--</div>--%>
                    <%--<br class="clear"/>--%>
                    <%--<div class="row-eq-height">--%>
                        <%--<c:forEach var="submission" items="${voteCandidates}">--%>
                            <%--<div class="col-sm-3 thumbnail">--%>
                                <%--<div>--%>
                                    <%--<t:questSubmission submission="${submission}" videoAutoplay="true" />--%>
                                    <%--<p class="text-center">--%>
                                        <%--<t:button context="primary"><spring:message code="vote" /></t:button>--%>
                                    <%--</p>--%>
                                <%--</div>--%>
                            <%--</div>--%>
                        <%--</c:forEach>--%>
                    <%--</div>--%>
                <%--</div>--%>
            <%--</div>--%>
        <%--</c:if>--%>

        <div class="col-sm-6">
            <h3>
                <a href="<spring:url value="${contestURL}/quest/challenge/QuestGuide.pdf" />" class="pull-right">
                    <spring:message var="exportMsg" code="quest.challenge.export.title" />
                    <t:faIcon icon="print" title="${exportMsg}" />
                </a>
                <spring:message code="quest.challenge.title" />
            </h3>
            <c:if test="${not empty challenges}">
                <div id="quest-challanges-carousel" class="carousel slide" data-ride="carousel" data-interval="20000">
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner">
                        <c:forEach var="challenge" items="${challenges}" varStatus="status">
                            <div class="item ${status.index == 0 ? 'active' : ''}">
                                <div class="challenge-headline">
                                    <c:set var="showName" scope="request" value="true"/>
                                    <%@ include file="/WEB-INF/views/quest/fragment/challengeQuickInfo.jsp" %>
                                </div>
                                <c:if test="${not empty challenge.imageURL}">
                                    <img src="${challenge.imageURL}" alt="${challenge.name}" class="center-block img-responsive" onerror='this.style.display = "none"'>
                                </c:if>
                                <div class="challenge-description">
                                    <p>${challenge.description}</p>
                                    <div class="text-center">
                                        <button class="btn btn-primary" onclick="showParticipateChallenge('${challenge.hashtag}', '${challenge.name}')">
                                            <spring:message code="quest.participate.btn.withPoints" arguments="${challenge.defaultPoints}" />
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </c:forEach>
                    </div>

                    <!-- Controls -->
                    <c:if test="${challenges.size() gt 1}">
                        <a class="left carousel-control" href="#quest-challanges-carousel" data-slide="prev"> <span class="glyphicon glyphicon-chevron-left"></span></a>
                        <a class="right carousel-control" href="#quest-challanges-carousel" data-slide="next"> <span class="glyphicon glyphicon-chevron-right"></span>
                    </c:if>
                </a>
                </div>

                <%@ include file="/WEB-INF/views/quest/fragment/participateChallengeModal.jsp"%>
            </c:if>
            <c:if test="${empty challenges}">
                <div class="no-items-available">
                    <spring:message code="quest.challenge.noResult" />
                </div>
            </c:if>
        </div>

        <div class="col-sm-6">
            <h3><spring:message code="quest.feed.title" /></h3>

            <%@ include file="/WEB-INF/views/timeline/timelineNotificationTemplates.jsp"%>
            <div id="timeline-body">
                <c:forEach items="${notifications}" var="notification">
                    <t:notification notification="${notification}" />
                </c:forEach>
            </div>
        </div>

    </jsp:body>
</t:template>
