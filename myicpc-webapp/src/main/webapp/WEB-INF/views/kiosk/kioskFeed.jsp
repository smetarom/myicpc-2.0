<%@ include file="/WEB-INF/views/includes/taglibs.jsp" %>

<t:templateKiosk>
    <jsp:attribute name="head">
        <link rel="stylesheet" href="<c:url value='/js/react-builds/css/main.ec868cab.css'/>" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500" rel="stylesheet">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.1.0/css/font-awesome.min.css">
    </jsp:attribute>
    <jsp:attribute name="javascript">
        <script src="<c:url value='/js/angular/masonry.pkgd.min.js'/>"></script>
        <script src="<c:url value='/js/angular/angular-masonry.min.js'/>"></script>
        <script src="<c:url value='/js/angular/imagesloaded.pkgd.min.js'/>"></script>
        <script src="<c:url value='/js/myicpc/controllers/kiosk.js'/>"></script>

        <script type="text/javascript">
            updateKioskPage = function(data) {
                if (data.type === 'modeChange') {
                    window.location.href='<spring:url value="${contestURL}/kiosk/calendar" />'
                }
            };

            var notifications = [];
            notifications = ${not empty notificationsJSON ? notificationsJSON : '[]'};

            notifications = notifications.length > 12 ? notifications.slice(notifications.length - 12, notifications.length) : notifications;

            $(function() {
                startSubscribe('${r.contextPath}', '${contest.code}', 'kiosk', updateKioskPage, null);
            });

        </script>
        <script src="<c:url value='/js/react-builds/js/main.7a4326bc.js'/>"></script>
    </jsp:attribute>

    <jsp:body>
        <div id="kiosk-content" style="padding: 0;">
            <!-- react mounts here into id="root"-->
            <div id="root"/>
        </div>
    </jsp:body>
</t:templateKiosk>
