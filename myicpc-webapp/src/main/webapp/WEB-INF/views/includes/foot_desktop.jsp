<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<script type="text/javascript">


   $(function () {
		
	    $(".dropdown").hover(
            function () {
	               // $('.dropdown-menu', this).stop(true, true).slideDown("fast");  // kills click
	              	$(this).addClass('open');
            },
            function () {
                	
		             // $('.dropdown-menu', this).stop(true, true).slideUp("fast"); // kills click
		              $(this).removeClass('open');
            } );

    var timediff = ${empty contestTime ? 0 : contestTime};

    function contestTime() {
      $(".time_holder").html(formatContestTime(timediff));
      timediff += 60;
    }
    function smallScreen() {
    	return $(window).width() < 768
    }

    contestTime();
    if (timediff < 18000) {
      setInterval(contestTime, 60 * 1000);
    }
  });
  
</script>