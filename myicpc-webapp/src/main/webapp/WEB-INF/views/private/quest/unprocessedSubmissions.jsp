<%@ include file="/WEB-INF/views/includes/taglibs.jsp"%>

<t:templateAdmin>
    <jsp:attribute name="title">
		<spring:message code="questAdmin.unprocessedSubmissions.title" />
	</jsp:attribute>

    <jsp:attribute name="headline">
		<spring:message code="questAdmin.unprocessedSubmissions.title" />
	</jsp:attribute>

    <jsp:attribute name="breadcrumb">
        <li><a href="<spring:url value="/private${contestURL}/quest/challenges" />"><spring:message code="nav.admin.quest" /></a></li>
	    <li class="active"><spring:message code="nav.admin.quest.unprocessedSubmissions" /></li>
	</jsp:attribute>


    <jsp:body>
        <div class="row">
            <div class="col-sm-12">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th><spring:message code="unprocessedSubmissions.username" /></th>
                        <th><spring:message code="unprocessedSubmissions.quest" /></th>
                        <th><spring:message code="unprocessedSubmissions.questStart" /></th>
                        <th><spring:message code="unprocessedSubmissions.submissionDate" /></th>
                        <th><spring:message code="unprocessedSubmissions.questEnd" /></th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach var="submission" items="${unprocessedSubmissions}">
                        <tr>
                            <td>${submission.username}</td>
                            <td>${submission.challengeName}</td>
                            <td><fmt:formatDate type="both" value="${submission.challengeStart}" /></td>
                            <td><fmt:formatDate type="both" value="${submission.notificationDate}" /></td>
                            <td><fmt:formatDate type="both" value="${submission.challengeEnd}" /></td>

                            <td class="text-right">
                                <a href="<spring:url value="/private/${contestURL}/quest/unprocessedSubmission/${submission.submissionId}/${submission.challengeId}" />" class="btn btn-default btn-xs">
                                    Process</a>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </jsp:body>
</t:templateAdmin>