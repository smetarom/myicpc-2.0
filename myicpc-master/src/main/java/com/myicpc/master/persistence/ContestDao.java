package com.myicpc.master.persistence;

import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * @author Roman Smetana
 */
@Service
public class ContestDao {
    @PersistenceContext
    private EntityManager em;

    public List<BigInteger> getActiveContestIds() {
        return em.createNativeQuery("SELECT id FROM Contest WHERE starttime > (CURRENT_TIMESTAMP + INTERVAL '-7 day')").getResultList();
    }
}
