package com.myicpc.master.service;

import com.myicpc.dto.jms.JMSEvent;
import com.myicpc.master.persistence.ContestDao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 * @author Roman Smetana
 */
@Service
public class TaskSchedulerService {
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
    private static final Logger logger = LoggerFactory.getLogger(TaskSchedulerService.class);

    @Autowired
    private JMSSenderService jmsSenderService;

    @Autowired
    private ContestDao contestDao;

    private static final JMSEvent.EventType[] everyMinuteSocialEvents = {JMSEvent.EventType.INSTAGRAM,
            JMSEvent.EventType.VINE,
            JMSEvent.EventType.POLL_OPEN,
            JMSEvent.EventType.ADMIN_NOTIFICATION};
    private static final JMSEvent.EventType[] everyMinuteScheduleEvents = {JMSEvent.EventType.SCHEDULE_OPEN_EVENT};
    private static final JMSEvent.EventType[] everyMinuteQuestEvents = {JMSEvent.EventType.QUEST_CHALLENGE,
            JMSEvent.EventType.QUEST_SUBMISSIONS};
    private static final JMSEvent.EventType[] everyMinuteCodeInsightEvents = {JMSEvent.EventType.CODE_INSIGHT_EVENT};


    private static final JMSEvent.EventType[] everyHourCmEventTypes = {JMSEvent.EventType.CM_SYNC_EVENT};
    private static final JMSEvent.EventType[] everyHourQuestEventTypes = {JMSEvent.EventType.QUEST_SUBMISSIONS_FULL};

    @PostConstruct
    private void init() {
    	logger.warn("[--- MASTER ---] init");
        startTwitterStreams();
    }

    public void startTwitterStreams() {
    	logger.warn("[--- MASTER ---] TWITTER start");
        for (BigInteger contestId : contestDao.getActiveContestIds()) {
            jmsSenderService.sendSocialMessage(new JMSEvent(contestId.longValue(), JMSEvent.EventType.TWITTER));
        }
    }

    @Scheduled(cron = "0 */1 * * * *")
    public void everyMinuteTasks() {
    	logger.warn("[--- MASTER ---] cron");
        List<BigInteger> activeContestIds = contestDao.getActiveContestIds();
        for (BigInteger contestId : activeContestIds) {
            for (JMSEvent.EventType everyMinuteSocialEvent : everyMinuteSocialEvents) {
                jmsSenderService.sendSocialMessage(new JMSEvent(contestId.longValue(), everyMinuteSocialEvent));
            }
            for (JMSEvent.EventType everyMinuteQuestEvent : everyMinuteQuestEvents) {
                jmsSenderService.sendQuestMessage(new JMSEvent(contestId.longValue(), everyMinuteQuestEvent));
            }
            for (JMSEvent.EventType everyMinuteScheduleEvent : everyMinuteScheduleEvents) {
                jmsSenderService.sendScheduleMessage(new JMSEvent(contestId.longValue(), everyMinuteScheduleEvent));
            }
            for (JMSEvent.EventType everyMinuteCodeInsightEvent : everyMinuteCodeInsightEvents) {
                jmsSenderService.sendCodeInsightMessage(new JMSEvent(contestId.longValue(), everyMinuteCodeInsightEvent));
            }
        }
    }

    @Scheduled(cron = "0 0 */1 * * *")
    public void everyHourTasks() {
        List<BigInteger> activeContestIds = contestDao.getActiveContestIds();
        for (BigInteger contestId : activeContestIds) {
            for (JMSEvent.EventType everyHourCmEventType : everyHourCmEventTypes) {
                jmsSenderService.sendCMSynchronizationMessage(new JMSEvent(contestId.longValue(), everyHourCmEventType));
            }
            for (JMSEvent.EventType everyHourQuestEventType : everyHourQuestEventTypes) {
                jmsSenderService.sendCodeInsightMessage(new JMSEvent(contestId.longValue(), everyHourQuestEventType));
            }
        }
    }
}
