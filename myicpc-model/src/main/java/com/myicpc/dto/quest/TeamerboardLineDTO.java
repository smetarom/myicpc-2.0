package com.myicpc.dto.quest;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import com.myicpc.model.quest.QuestSubmission;

/**
 * @author Tomas
 */
public class TeamerboardLineDTO implements Serializable {

	private static final long serialVersionUID = -2819680806772894L;
	
	private Long teamId;
	private String teamName;
    private int acceptedSubmissions = 0;
    private int solved = 0;
    private int points = 0;
    // challenge id
    private final Map<Long, QuestSubmissionDTO> submissionMap = new HashMap<>();

    //adapter
    public Adapter getContestParticipant() {
    	return new Adapter(this);
    }
    
    public static class Adapter {
    	private TeamerboardLineDTO dto;
    	protected Adapter(TeamerboardLineDTO dto) {
    		this.dto = dto;
    	}
    	public String getFullname() {
    		return dto.teamName;
    	}
    	public Long getId() {
    		return dto.teamId;
    	}
    }
    
    public int getTotalPoints() {
    	return points;
    }

    public void setTotalPoints(final int points) {
        this.points = points;
    }

	public int getAcceptedSubmissions() {
		return acceptedSubmissions;
	}

	public Map<Long, QuestSubmissionDTO> getSubmissionMap() {
		return submissionMap;
	}
	
	public void process(TeamerboardLineColumnDTO dto) {
		
		if(!submissionMap.containsKey(dto.getQuestChallengeId())) {
			QuestSubmissionDTO newQuest = makeQuestSubmissionDTO(dto);
			submissionMap.put(newQuest.getQuestChallengeId(), newQuest);
			if (newQuest.getSubmissionState().equals(QuestSubmission.QuestSubmissionState.ACCEPTED)) {
				solved++;
				acceptedSubmissions++;
				points += dto.getQuestPoints();
			}
		} else {
			QuestSubmissionDTO existingQuest = submissionMap.get(dto.getQuestChallengeId());
			if (existingQuest.getSubmissionState().equals(QuestSubmission.QuestSubmissionState.ACCEPTED)) {
				return;
			} 
			QuestSubmissionDTO newQuest = makeQuestSubmissionDTO(dto);
			submissionMap.put(newQuest.getQuestChallengeId(), newQuest);
			if (newQuest.getSubmissionState().equals(QuestSubmission.QuestSubmissionState.ACCEPTED)) {
				solved++;
				acceptedSubmissions++;
				points += dto.getQuestPoints();
			}
		}
	}

	private QuestSubmissionDTO makeQuestSubmissionDTO(TeamerboardLineColumnDTO dto) {
		QuestSubmissionDTO newQuest = new QuestSubmissionDTO(dto.getTeamInfoId(), dto.getQuestChallengeId(), dto.getSubmissionState(), dto.getReasonToReject());
		return newQuest;
	}

	public Long getTeamId() {
		return teamId;
	}

	public String getTeamName() {
		return teamName;
	}

	public TeamerboardLineDTO(Long teamId, String teamName) {
		super();
		this.teamId = teamId;
		this.teamName = teamName;
	}
}
