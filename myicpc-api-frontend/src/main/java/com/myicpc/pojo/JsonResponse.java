package com.myicpc.pojo;

import com.myicpc.enums.HTTPStatusCode;
import com.myicpc.enums.CodeSuccess;

import java.io.Serializable;

/**
 * Note that error and payload should not co-exist in the same JSON response. Be careful when picking HTTPStatusCode.
 * For example, if you use some server error code like 500 even if request has been successful and you send a payload,
 * the client will ignore your payload and think that his request errored.
 * We consider error as everything that is not an instance of CodeSuccess.
 *
 * Created by Robert Cepa on 4/13/17.
 */
public class JsonResponse<T> implements Serializable {

    private static final long serialVersionUID = -8564454680218504427L;
    private int code;
    private String message;
    private Boolean error;
    private T payload;

    public JsonResponse(HTTPStatusCode status, T payload) {
        this.payload = payload;
        this.error = !(status instanceof CodeSuccess);
        this.message = status.getLabel();
        this.code = status.getCode();
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public T getPayload() {
        return payload;
    }

    public void setPayload(T payload) {
        this.payload = payload;
    }
}
