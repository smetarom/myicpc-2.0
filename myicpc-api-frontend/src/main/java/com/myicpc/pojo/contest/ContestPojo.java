package com.myicpc.pojo.contest;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.DateSerializer;
import com.myicpc.model.contest.Contest;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Robert Cepa on 12/04/2017.
 */
public class ContestPojo implements Serializable {
    private static final long serialVersionUID = 1996358721379484790L;

    private String name;
    private String shortName;
    private String code;
    private Date startTime;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @JsonSerialize(using = DateSerializer.class)
    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }
}
