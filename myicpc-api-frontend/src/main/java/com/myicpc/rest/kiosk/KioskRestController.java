package com.myicpc.rest.kiosk;

import com.myicpc.enums.CodeClientError;
import com.myicpc.enums.CodeSuccess;
import com.myicpc.factory.notification.NotificationPojoFactory;
import com.myicpc.model.contest.Contest;
import com.myicpc.model.social.Notification;
import com.myicpc.pojo.JsonResponse;
import com.myicpc.pojo.notification.NotificationPojo;
import com.myicpc.service.contest.ContestService;
import com.myicpc.service.exception.ContestNotFoundException;
import com.myicpc.service.kiosk.KioskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Robert Cepa on 4/13/17.
 */
@RestController
@RequestMapping("/contests/{contestCode}/kiosk")
public class KioskRestController {
    @Autowired
    KioskService kioskService;

    @Autowired
    ContestService contestService;

    @RequestMapping(value = "/notifications" , method = RequestMethod.GET, produces = "application/json")
    public JsonResponse<List<NotificationPojo>> kioskNotifications(@PathVariable String contestCode, @RequestParam(value = "count", required = false) Integer count) {
        try {
            Contest contest = contestService.getContest(contestCode);
            List<Notification> notifications = kioskService.getKioskNotifications(contest);
            int cnt = (count == null || count > notifications.size()) ? notifications.size() : count;
            return new JsonResponse<>(CodeSuccess.OK, NotificationPojoFactory.notificationPojoListFactory(notifications.subList(notifications.size() - cnt, notifications.size())));
        } catch (ContestNotFoundException e) {
            return new JsonResponse<>(CodeClientError.NOT_FOUND, null);
        }
    }
}
