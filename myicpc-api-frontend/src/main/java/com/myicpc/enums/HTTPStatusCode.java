package com.myicpc.enums;

/**
 * Interface for HTTP codes being sent by JsonResponse
 * We don't keep all possible codes here, only the ones used by our app
 * Inspired by http://www.restapitutorial.com/httpstatuscodes.html
 *
 * Created by Robert Cepa on 15/04/2017.
 */
public interface HTTPStatusCode {
    int getCode();
    String getLabel();
}
