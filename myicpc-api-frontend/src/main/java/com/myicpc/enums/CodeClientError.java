package com.myicpc.enums;

/**
 * Created by Robert Cepa on 15/04/2017.
 */
public enum CodeClientError implements HTTPStatusCode {
    BAD_REQUEST(400, "Bad request"),
    UNAUTHORIZED(401, "Unauthorized"),
    FORBIDDEN(403, "Forbidden"),
    NOT_FOUND(404, "Not found"),
    CONFLICT(409, "Conflict");

    /**
     * The HTTP status code
     */
    private final int code;

    /**
     * Brief desciption of the given status code
     */
    private final String label;

    CodeClientError(final int code, final String label) {
        this.code = code;
        this.label = label;
    }

    @Override
    public int getCode() {
        return code;
    }

    @Override
    public String getLabel() {
        return label;
    }
}
