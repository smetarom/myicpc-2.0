package com.myicpc.factory.contest;

import com.myicpc.model.contest.Contest;
import com.myicpc.pojo.contest.ContestPojo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Robert Cepa on 12/04/2017.
 */
public class ContestPojoFactory {

    public static ContestPojo contestPojoFactory(Contest contest) {
        ContestPojo contestPojo = new ContestPojo();
        contestPojo.setCode(contest.getCode());
        contestPojo.setName(contest.getName());
        contestPojo.setShortName(contest.getShortName());
        contestPojo.setStartTime(contest.getStartTime());
        return contestPojo;
    }

    public static List<ContestPojo> contestPojoListFactory(List<Contest> contests) {
        List<ContestPojo> contestPojoList = new ArrayList<>();
        for (Contest contest : contests) {
            contestPojoList.add(contestPojoFactory(contest));
        }
        return contestPojoList;
    }
}
