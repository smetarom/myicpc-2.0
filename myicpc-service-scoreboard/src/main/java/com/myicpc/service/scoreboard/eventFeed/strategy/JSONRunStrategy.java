package com.myicpc.service.scoreboard.eventFeed.strategy;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.myicpc.commons.adapters.JSONAdapter;
import com.myicpc.commons.utils.FormatUtils;
import com.myicpc.commons.utils.WebServiceUtils;
import com.myicpc.model.ErrorMessage;
import com.myicpc.model.contest.Contest;
import com.myicpc.model.eventFeed.Team;
import com.myicpc.model.eventFeed.TeamProblem;
import com.myicpc.service.notification.ErrorMessageService;
import com.myicpc.service.scoreboard.exception.EventFeedException;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Scoreboard strategy, which uses JSON scoreboard snapshot to get current state
 * and update MyICPC scoreboard state based on this JSON
 *
 * @author Roman Smetana
 */
@Component
public class JSONRunStrategy extends FeedRunStrategy {
    private static final Logger logger = LoggerFactory.getLogger(JSONRunStrategy.class);

    @Autowired
    private ErrorMessageService errorMessageService;

    /**
     * Uses scoreboard JSON snapshot
     * <p/>
     * Gets the full snapshot of the current scoreboard and updates the changes
     * to get the current state
     *
     * @param teamProblem team submission
     * @param contest     contest
     * @return list of changed teams
     * @throws EventFeedException error in getting snapshot or JSON parsing error
     */
    @Override
    protected List<Team> processScoreboardChanges(TeamProblem teamProblem, Contest contest) throws EventFeedException {
        List<Team> teamsToBroadcast = new ArrayList<>();
        try {
            // Get all teams
            Iterable<Team> teams = teamRepository.findByContest(contest);

            // parse JSON scoreboard
            JsonArray arr = new JsonParser().parse(scoreboardService.getJSONContent(contest)).getAsJsonArray();
            

            Iterator<JsonElement> jsonIterator = arr.iterator();
            // Mapping between team and its ID, for faster lookup
            Map<Long, Team> teamMap = new HashMap<>();
            for (Team team : teams) {
                teamMap.put(team.getSystemId(), team);
            }

            while (jsonIterator.hasNext()) {
                JSONAdapter jsonAdapter = new JSONAdapter(jsonIterator.next());
                Team team = teamMap.get(jsonAdapter.getLong("team"));

                // throw exception if team is not found by ID
                if (team == null) {
                    throw new EventFeedException("JSON event feed file contains a team, which is not in database.");
                }

                processTeamSubmission(team, teamProblem, jsonAdapter, teamsToBroadcast);
            }
            //teamRepository.save(teams);
            teamRepository.save(teamsToBroadcast);
        } catch (Throwable ex) {
            logger.error(ex.getMessage(), ex);
            //throw new EventFeedException("Unknown problem with the parsing of the json scoreboard.", ex);
        }
        return teamsToBroadcast;
    }

    private void processTeamSubmission(final Team team, final TeamProblem teamProblemFromDB, final JSONAdapter teamAdapter,
                                         final List<Team> teamsToBroadcast) throws EventFeedException {
        int oldRank = team.getRank();
        team.setRank(teamAdapter.getInteger("rank"));

        // mark team, if the rank differs
        if (oldRank != team.getRank()) {
            teamsToBroadcast.add(team);
        }

        // this is the team, who submitted
        if (team.getId().equals(teamProblemFromDB.getTeam().getId())) {
            JsonElement scoreObject = teamAdapter.get("score").getAsJsonObject();
            JSONAdapter scoreAdapter = new JSONAdapter(scoreObject);
            Integer solved = scoreAdapter.getInteger("num_solved");
            Integer score = scoreAdapter.getInteger("total_time");
            if (oldRank == team.getRank() && (team.getProblemsSolved() != solved || team.getTotalTime() != score)) {
            	teamsToBroadcast.add(team);
            }
            team.setProblemsSolved(solved);
            teamProblemFromDB.getTeam().setProblemsSolved(solved);
            team.setTotalTime(score);
            teamProblemFromDB.getTeam().setTotalTime(score);            
            JsonArray problems = teamAdapter.getJsonArray("problems");
            
            boolean processed = false;
            for (JsonElement jsonElement : problems) {
            	JSONAdapter problemAdapter = new JSONAdapter(jsonElement);
            	if (problemAdapter.getString("label").equals(teamProblemFromDB.getProblem().getCode())) {
            		processed = true;
                    teamProblemFromDB.setAttempts(problemAdapter.getInteger("num_judged"));
                    if(problemAdapter.getBoolean("solved")) {
                    	if (teamProblemFromDB.getTime() == null) {  // Save from feed, warning: scoreboard has time in minutes, event feed has time in milliseconds
                    		teamProblemFromDB.setTime(problemAdapter.getDouble("time") * 60);
                    	}
	                    if(problemAdapter.getBoolean("first_to_solve")) {
	                    	removeFirstSolvedProblem(teamProblemFromDB.getProblem());
	                    	teamProblemFromDB.setFirstSolved(true);
	                    }
                    }
                    break;
            	}
			}
            if (!processed) {
            		//throw new EventFeedException("Problem was not found");
            		logger.warn("Problem was not found: " + teamProblemFromDB.getProblem().getCode());
            }
        }
    }
}