package com.myicpc.controller.quest.admin;

import com.myicpc.controller.GeneralAdminController;
import com.myicpc.model.contest.Contest;
import com.myicpc.model.quest.QuestChallenge;
import com.myicpc.model.social.Notification;
import com.myicpc.repository.quest.QuestChallengeRepository;
import com.myicpc.repository.quest.QuestSubmissionRepository;
import com.myicpc.repository.social.NotificationRepository;
import com.myicpc.service.quest.QuestSubmissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 * Created by Robert Cepa on 5/9/17.
 */
@Controller
@SessionAttributes("unprocessedSubmissions")
public class QuestUnprocessedSubmissionAdminController extends GeneralAdminController {
    @Autowired
    private QuestSubmissionService questSubmissionService;

    @Autowired
    private NotificationRepository notificationRepository;

    @Autowired
    private QuestChallengeRepository questChallengeRepository;

    @RequestMapping(value = "/private/{contestCode}/quest/unprocessedSubmissions", method = RequestMethod.GET)
    public String unprocessedSubmissions(@PathVariable final String contestCode, Model model) {
        Contest contest = getContest(contestCode, model);

        model.addAttribute("unprocessedSubmissions", questSubmissionService.getUnprocessedSubmissions(contest));

        return "private/quest/unprocessedSubmissions";
    }

    @RequestMapping(value = "/private/{contestCode}/quest/unprocessedSubmission/{submissionId}/{challengeId}", method = RequestMethod.GET)
    public String processSubmission(@PathVariable final String contestCode, @PathVariable final Long submissionId, @PathVariable final Long challengeId, Model model, final RedirectAttributes redirectAttributes) {
        Contest contest = getContest(contestCode, model);
        Notification submission = notificationRepository.findOne(submissionId);
        QuestChallenge challenge = questChallengeRepository.findOne(challengeId);

        questSubmissionService.processUnprocessedSubmission(contest, submission, challenge);
        model.addAttribute("unprocessedSubmissions", questSubmissionService.getUnprocessedSubmissions(contest));

        successMessage(redirectAttributes, "questAdmin.unprocessedSubmissions.processed", submission.getAuthorUsername());
        return "redirect:/private" + getContestURL(contestCode) + "/quest/unprocessedSubmissions";
    }

}
